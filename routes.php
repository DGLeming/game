<?php 
	$routes = array(
			"/"	=> "handler_home",
			"/welcome"	       => "handler_welcome",
			"/signup"	       => "handler_signup",
			"/signin"          => "handler_signin",
			"/shop"            => "handler_shop",
			"/shop/buy/%"      => "handler_shop_buy",
			"/storage"         => "handler_storage",
			"/storage/collect" => "handler_storage_collect"

		);
