<?php

	session_start();

	function redirect_to ($url) {
		header("Location: {$url}");
		exit();
	}

	function _get ($key) {
		if (array_key_exists($key, $_GET))
			return $_GET[$key];
		else
			return null;
	}

	function _post ($key) {
		if (array_key_exists($key, $_POST))
			return $_POST[$key];
		else
			return null;
	}

	function add_notification ($message) {
		if (!(array_key_exists("notifications", $_SESSION) && is_array($_SESSION['notifications']))) {
				$_SESSION["notifications"] = array();
		}
		$_SESSION['notifications'][] = $message;
	}

	function get_notifications () {
		if (array_key_exists("notifications", $_SESSION) && is_array($_SESSION['notifications'])) {
			$notifications = $_SESSION['notifications'];
			unset($_SESSION['notifications']);
			return $notifications;
		}
	}

	function is_logged () {
		return (array_key_exists("logged", $_SESSION) && $_SESSION['logged'] == true);
	}

	function theme ($theme_file, $theme_data = array()) {
		extract($theme_data);
		ob_start();
		require("templates/{$theme_file}");
		return ob_get_clean();
	}

	require_once("routes.php");

	require_once("db.php");

	require_once("helpers/helper_profile.php");
	require_once("helpers/helper_signin.php");
	require_once("helpers/helper_signup.php");
	require_once("helpers/helper_notifications.php");

	$html_content = null;

	$request_uri = $_SERVER["REQUEST_URI"]; 

	foreach($routes as $URI => $function){
		$regexp = "/^" . str_replace("%", "([^\/]*)", preg_quote($URI, '/')) . "\/?$/";
		if(preg_match($regexp, $request_uri, $matches)){
			require("handlers/{$function}.php");
			$matches = array_slice($matches, 1);
			$html_content = call_user_func_array($function, $matches);
		}
	}

	if (is_null($html_content)) {
		$html_content = theme("page404.tpl.php");
	}

	echo theme("layout.tpl.php", array("content" => $html_content));
	