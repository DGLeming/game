<script>
function change(idName) {
  if(document.getElementById(idName).style.display=='none') {
    document.getElementById(idName).style.display = '';
  } else {
    document.getElementById(idName).style.display = 'none';
  }
  return false;
}
</script>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Online minner - главная</title>
<link rel="stylesheet" type="text/css" href="/style/style.css" />\

<script type="text/javascript" src="//vk.com/js/api/openapi.js?115"></script>

<script type="text/javascript">
  VK.init({apiId: 4497215, onlyWidgets: true});
</script>

</head>

<body>

<div id="top-line"></div>
<div id="header">
	<a href="#" id="logo"></a>
	<div id='call1'>mcleming.ru</br>мы с вами</div>
	<div id="top-menu">
		<a href="#">Главная</a>
		<div class="top-menu-line">|</div>
		<a href="#">Статистика</a>
		<div class="top-menu-line">|</div>
		<a href="#">Помощь</a>
		<div class="top-menu-line">|</div>
		<a href="#">Обновления</a>
		<div class="top-menu-line">|</div>
		<a href="#">Чат</a>
		<div class="top-menu-line">|</div>
		<a href="#">TOP</a>
		<div class="top-menu-line">|</div>
		<a href="#">Конкурсы</a>
	</div>
	<div class="social">
		<div id="vk_like"></div>
		<script type="text/javascript">
		VK.Widgets.Like("vk_like", {type: "mini", height: 24});
		</script>
	</div>
</div>
<div id="content">

	<div id="leftblock">
		<?=helper_signin();?>
		<?=helper_signup();?>
		<?=helper_profile();?>
	</div>
	
	<div id="rightblock">
		<?=helper_notifications();?>
		<?=$content;?>
		
	</div>

</div>
</body>
</html>