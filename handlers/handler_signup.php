<?php

	function handler_signup () {
		global $pdo;

		$login     = _post('login');
		$mail      = _post('mail');
		$password  = _post('password');
		$rpassword = _post('rpassword');

		if ($password != $rpassword) {
			add_notification("Пароли не совпадают!");
			redirect_to("/");
		}

		if (_handler_signup_user_exist($login)) {
			add_notification("Пользователь с таким логином уже существует!");
			redirect_to("/");
		}		
		
		if (_handler_signup_mail_exist($mail)) {
			add_notification("Пользователь с таким e-mail'ом уже существует!");
			redirect_to("/");
		}

		$add = $pdo->prepare("INSERT INTO users (NICK_US,PASS,mail,alm,money,rab1,rab2,rab3,rab4,rab5,rab6) VALUES (:login,:password,:mail,'0','0','0','0','0','0','0','0')");

		$add->execute(array(":login" => $login, ':password' => $password, ':mail' => $mail));

		add_notification("Вы успешно зарегестрировались!");
		redirect_to("/");
	}

	function _handler_signup_user_exist ($login) {
		global $pdo;

		$select_user_exist = $pdo->prepare("SELECT COUNT(*) as count FROM users WHERE NICK_US = :login");

		$select_user_exist->execute(array(":login" => $login));

		return $select_user_exist->fetchColumn();
	}

	function _handler_signup_mail_exist ($mail) {
		global $pdo;

		$select_mail_exist = $pdo->prepare("SELECT COUNT(*) as count FROM users WHERE mail = :mail");

		$select_mail_exist->execute(array(":mail" => $mail));

		return $select_mail_exist->fetchColumn();
	}