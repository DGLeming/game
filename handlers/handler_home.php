<?php

	function handler_home () {
		
		if(is_logged()){
			redirect_to("/shop");
		}

		return theme("home/index.tpl.php");
	}