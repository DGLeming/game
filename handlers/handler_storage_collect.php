<?php

function handler_storage_collect () {
	$uid = $_SESSION['uid'];

	$user = get_user_info($uid);

	$collect = select_collect_sum($uid);

	user_update_grab_to_null($uid);

	user_update_diamonds($uid, $collect);

	add_notification("Вы собрали алмазы в количестве {$collect} шт.");
	redirect_to("/shop");
}

function get_user_info ($uid) {
	global $pdo;

	$user = $pdo->prepare("SELECT * FROM users WHERE id_us = ?");

	$user->execute(array($uid));

	return $user->fetch();
}

function select_collect_sum ($uid) {
	global $pdo;

	$select_collect_sum = $pdo->prepare("SELECT (grab1 + grab2 + grab3 + grab4 + grab5) as collect_sum FROM users WHERE id_us = :id");

	$select_collect_sum->execute(array(':id' => $uid));

	$collect_sum = $select_collect_sum->fetchColumn();

	return $collect_sum;
}

function user_update_grab_to_null ($uid) {
	global $pdo;

	$user_update = $pdo->prepare("UPDATE users SET grab1 = 0, grab2 = 0, grab3 = 0, grab4 = 0, grab5 = 0 WHERE id_us = :id");

	$user_update->execute(array(':id' => $uid));

}

function user_update_diamonds($uid, $collect) {
	global $pdo;

	$user_update_diamonds = $pdo->prepare("UPDATE users SET alm = :collect WHERE id_us = :id");
	
	$user_update_diamonds->execute(array(':collect' => $collect, ':id' => $uid));
}