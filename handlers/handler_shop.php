<?php

	function handler_shop () {
		$user = get_user_info($_SESSION['uid']);		

		return theme('shop/index.tpl.php', array('user' => $user));
	}

	function get_user_info ($uid) {
		global $pdo;
		$user = $pdo->prepare("SELECT * FROM users WHERE id_us = ?");
		$user->execute(array($uid));
		return $user->fetch();
	}