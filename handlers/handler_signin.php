<?php
function handler_signin () {
	$password  = _post('password');
	$login     = _post('login');

	$uid = _handler_signin_get_uid($login, $password);

	if (!$uid) {
		add_notification("Неправильный логин или пароль!");
		redirect_to("/");
	}

	$_SESSION['logged'] = true;
	$_SESSION['uid'] = $uid;
	add_notification("Вы успешно вошли!");
	redirect_to("/");
}

function _handler_signin_get_uid ($login,$password) {
	global $pdo;

	$signin = $pdo->prepare("SELECT id_us FROM users WHERE NICK_US = :login AND PASS = :password");

	$signin->execute(array(":login" => $login, ":password" => $password));

	return $signin->fetchColumn();
}