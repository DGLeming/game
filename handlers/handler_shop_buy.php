<?php

	function handler_shop_buy ($color_worker) {
		$user = get_user_info($_SESSION['uid']);

		$cost = null;

		switch ($color_worker) {
			case ("green") :
				$cost = 100;
				break;

			case ("yellow") :
				$cost = 1000;
				break;

			case ("brown") :
				$cost = 5000;
				break;

			case ("blue") :
				$cost = 25000;
				break;

			case ("red") :
				$cost = 100000;
				break;

			default: 
				add_notification("Неизвестный рабочий");
				redirect_to('/shop');
				break;
		}	

		if ($user['money'] < $cost) {
			add_notification("Не хватает денег. Нужно {$cost}.");
			redirect_to("/shop");
		}

		user_update_money($cost, $_SESSION['uid']);

		switch ($color_worker) {
			case 'green':
				user_update_worker_green($_SESSION['uid']);
				add_notification("Зеленый рабочий куплен успешно!");
				break;

			case 'yellow':
				user_update_worker_yellow($_SESSION['uid']);
				add_notification("Желтый рабочий куплен успешно!");
				break;

			case 'brown':
				user_update_worker_brown($_SESSION['uid']);
				add_notification("Коричневый рабочий куплен успешно!");
				break;

			case 'blue':
				user_update_worker_blue($_SESSION['uid']);
				add_notification("Синий рабочий куплен успешно!");
				break;

			case 'red':
				user_update_worker_red($_SESSION['uid']);
				add_notification("Красный рабочий куплен успешно!");
				break;
		}

		redirect_to("/shop");

	}

	function get_user_info ($uid) {
		global $pdo;
		$user = $pdo->prepare("SELECT * FROM users WHERE id_us = ?");
		$user->execute(array($uid));
		return $user->fetch();
	}

	function user_update_money ($cost, $uid) {
		global $pdo;
		$user_update_money = $pdo->prepare("UPDATE users SET money = money-:cost WHERE id_us = :id");
		$user_update_money->execute(array(':cost' => $cost, ':id' => $uid));
	}

	function user_update_worker_green ($uid) {
		global $pdo;
		$user_update_worker_green = $pdo->prepare("UPDATE users SET rab1 = rab1+1 WHERE id_us = :id");
		$user_update_worker_green->execute(array(':id' => $uid));
	}

	function user_update_worker_yellow ($uid) {
		global $pdo;
		$user_update_worker_yellow = $pdo->prepare("UPDATE users SET rab2 = rab2+1 WHERE id_us = :id");
		$user_update_worker_yellow->execute(array(':id' => $uid));
	}

	function user_update_worker_brown ($uid) {
		global $pdo;
		$user_update_worker_brown = $pdo->prepare("UPDATE users SET rab3 = rab3+1 WHERE id_us = :id");
		$user_update_worker_brown->execute(array(':id' => $uid));
	}

	function user_update_worker_blue ($uid) {
		global $pdo;
		$user_update_worker_blue = $pdo->prepare("UPDATE users SET rab4 = rab4+1 WHERE id_us = :id");
		$user_update_worker_blue->execute(array(':id' => $uid));
	}

	function user_update_worker_red ($uid) {
		global $pdo;
		$user_update_worker_red = $pdo->prepare("UPDATE users SET rab5 = rab5+1 WHERE id_us = :id");
		$user_update_worker_red->execute(array(':id' => $uid));
	}