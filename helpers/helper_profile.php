<?php

function helper_profile () {
	if (is_logged()) {
		$user = _helper_profile_get_user($_SESSION['uid']);
		return theme("user/short_profile.tpl.php", array('user' => $user));
	}
}

function _helper_profile_get_user ($uid) {
	global $pdo;

	$select_user = $pdo->prepare("SELECT * FROM users WHERE id_us = ?");

	$select_user->execute(array($uid));

	return $select_user->fetch();
}