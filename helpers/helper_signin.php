<?php

function helper_signin () {
	if (!is_logged()) {
		return theme("form/signin.tpl.php");
	}
}