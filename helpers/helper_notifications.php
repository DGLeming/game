<?php

	function helper_notifications () {
		$notifications = get_notifications();
		if (!empty($notifications)) {
			return theme('notifications.tpl.php', array('notifications' => $notifications)); 
		}
	}
